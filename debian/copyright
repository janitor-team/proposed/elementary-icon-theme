Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: elementary-icons
Source: https://github.com/elementary/icons

Files: *
Copyright:
 2021 Daniel Foré <daniel@elementaryos.org>
 2021 elementary LLC <contact@elementary.io>
License: GPL-3.0-or-later

Files:
 data/icons.appdata.xml.in
Copyright:
 2021 Daniel Foré <daniel@elementaryos.org>
 2021 elementary LLC <contact@elementary.io>
License: CC0-1.0

Files:
 actions/16/node-align-horizontal.svg
 actions/16/node-align-vertical.svg
 actions/16/node-cusp.svg
 actions/16/node-distribute-horizontal.svg
 actions/16/node-distribute-vertical.svg
 actions/16/node-insert.svg
 actions/16/node-smooth.svg
 actions/16/node-symmetric.svg
 actions/16/object-to-path.svg
 actions/16/segment-curve.svg
 actions/16/segment-line.svg
 actions/16/stroke-to-path.svg
Copyright:
 2021 Daniel Foré <daniel@elementaryos.org>
 2021 elementary LLC <contact@elementary.io>
License: GPL-2.0

Files:
 cursors/24/all-scroll.svg
 cursors/24/bd_double_arrow.svg
 cursors/24/bottom_tee.svg
 cursors/24/circle.svg
 cursors/24/copy.svg
 cursors/24/crosshair.svg
 cursors/24/cross.svg
 cursors/24/dnd-ask.svg
 cursors/24/dnd-copy.svg
 cursors/24/dnd-link.svg
 cursors/24/dnd-move.svg
 cursors/24/dnd-none.svg
 cursors/24/dotbox.svg
 cursors/24/fd_double_arrow.svg
 cursors/24/hand1.svg
 cursors/24/left_ptr.svg
 cursors/24/left_ptr_watch_0001.svg
 cursors/24/left_ptr_watch_0002.svg
 cursors/24/left_ptr_watch_0003.svg
 cursors/24/left_ptr_watch_0004.svg
 cursors/24/left_ptr_watch_0005.svg
 cursors/24/left_ptr_watch_0006.svg
 cursors/24/left_ptr_watch_0007.svg
 cursors/24/left_ptr_watch_0008.svg
 cursors/24/left_tee.svg
 cursors/24/link.svg
 cursors/24/ll_angle.svg
 cursors/24/lr_angle.svg
 cursors/24/move.svg
 cursors/24/no-drop.svg
 cursors/24/not-allowed.svg
 cursors/24/pencil.svg
 cursors/24/plus.svg
 cursors/24/pointer.svg
 cursors/24/right_ptr.svg
 cursors/24/right_tee.svg
 cursors/24/sb_down_arrow.svg
 cursors/24/sb_h_double_arrow.svg
 cursors/24/sb_left_arrow.svg
 cursors/24/sb_right_arrow.svg
 cursors/24/sb_up_arrow.svg
 cursors/24/sb_v_double_arrow.svg
 cursors/24/tcross.svg
 cursors/24/text.svg
 cursors/24/top_tee.svg
 cursors/24/ul_angle.svg
 cursors/24/ur_angle.svg
 cursors/24/vertical-text.svg
 cursors/24/watch_0001.svg
 cursors/24/watch_0002.svg
 cursors/24/watch_0003.svg
 cursors/24/watch_0004.svg
 cursors/24/watch_0005.svg
 cursors/24/watch_0006.svg
 cursors/24/watch_0007.svg
 cursors/24/watch_0008.svg
 cursors/24/watch_0009.svg
 cursors/24/watch_0010.svg
 cursors/24/watch_0011.svg
 cursors/24/watch_0012.svg
 cursors/24/watch_0013.svg
 cursors/24/watch_0014.svg
 cursors/24/watch_0015.svg
 cursors/24/watch_0016.svg
 cursors/24/X_cursor.svg
Copyright:
 2021 Daniel Foré <daniel@elementaryos.org>
 2021 elementary LLC <contact@elementary.io>
License: Expat

Files: debian/*
Copyright:
 2022 Boyuan Yang <byang@debian.org>
License: CC0-1.0

License: GPL-3.0-or-later
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, see <https://www.gnu.org/licenses/>.
 .
 On Debian systems, the full text of GPL-3 could be found at
 `/usr/share/common-licenses/GPL-3'.

License: CC0-1.0
 On Debian systems, the full text of the CC0 1.0 Universal License
 can be found in the file `/usr/share/common-licenses/CC0-1.0'.

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

License: GPL-2.0
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, see <https://www.gnu.org/licenses/>.
 .
 On Debian systems, the full text of GPL-2 could be found at
 `/usr/share/common-licenses/GPL-2'.
